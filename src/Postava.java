import java.util.Scanner;

public class Postava {

    private String jmeno;
    private int zivoty = 120;
    private int sila = 80;
    private int energie = 80;

    public void setJmeno(){
        Scanner Postava = new Scanner(System.in);
        System.out.println("Zadej nové jméno postavy: ");
        this.jmeno = Postava.nextLine();
    }
    public void setPCJmeno(){
        this.jmeno = "PC";
    }

    public int utok(){
        int utok = (10 * this.sila) / this.energie;
        return utok;
    }

    public int getZivoty() {
        return this.zivoty;
    }

}
